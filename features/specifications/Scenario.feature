Feature: Check Tasks page and features.

Scenario: TCUS001P001-Check if the 'My Tasks' link is visible and working from 'Main Page' and 'My tasks' pages.

  Given I access 'Tasks' main screen
  When I log into 'Tasks'
  And 'My Tasks' link is visible from 'Main Page'
  And 'My Tasks' link is working
  And 'My Tasks' link is visible from 'My Tasks' page
  Then 'My Tasks' link is working from 'My Tasks' page.

Scenario: TCUS001P005-Check if a new task can be added by clicking on 'Add Task' button.

  Given I want to add a new Task by clicking 'Add Task' button
  When I add a valid task name and click 'Add Task' button
  Then the task should be added to tasks list.

Scenario: TCUS001P006-Check if a new task can be added by hitting 'Enter' key.
  
  Given I want to add a new task by hitting 'Enter' key
  When I add a valid task name and hit 'Enter' key
  Then the new task should be added to tasks list.

Scenario: TCUS001P009-Check if the message "Hey 'user', this is your todo list for today:' is displayed on 'My Tasks' page".

  Given I want to check welcome message
  Then the message "Hey 'user', this is your todo list for today:' is displayed on 'My Tasks' page.

Scenario: TCUS001N001-Check if a task is not inserted if the new task title have less then 3 characters.
  
  Given I want to add title task name less then 3 characters 
  When I add a task name with less then 3 characters and click 'Add Task' button
  Then the task should not be added to tasks list.

Scenario: TCUS001N002-Check if a task is not inserted if the new task title have more then 250 characters.
  
  Given I want to add title task name bigger then 250 characters
  When I add a task name with more then 250 characters and click 'Add Task' button
  Then the task should not be displayed in tasks list.
