Feature: Check subtasks features

Scenario: TCUS002P001-Check if the button 'Manage subtasks' is visible from a created task and shows '0' count.

  Given I want to check if 'Manage subtasks' is visible for a task
  When I create a task
  And the 'Manage subtasks' button is visible
  Then the subtasks count is 0

Scenario: TCUS002P003-Check if the popup window 'Manage subtasks' is displayed when clicking on 'Manage Subtasks' button.

  Given I want to see if the 'Manage subtasks' popup is displayed
  When I click in 'Manage Subtasks' button
  Then the 'Manage subtasks' popup is displayed

Scenario: TCUS002P005-Check if the 'Task Description' field is read only and matches Task Description.

  Given I want to see if 'Task Description' is read only and matches 'Task Description'
  When I click 'Manage Subtasks'
  And 'Task Description' is read only
  Then 'Task Description' matches his 'Task Description'

Scenario: TCUS002P008-Create a new subtask from a previously created Task by checking bottom part of the 'Manage subtasks' modal.
  
  Given I want to create a new Subtask
  When I click 'Manage Subtasks' and popup is displayed
  And I fill 'Subtask Description'
  And I fill 'Subtask Due Date'
  And I click 'Add'
  Then The new Subtask should be displayed in the bottom part of the modal.

Scenario: TCUS002N001-Check if no SubTask is added when its name have more then 250 characters.
  Given I want to check if invalid SubTask name avoids SubTask creation
  When I fill an incorrect SubTask name
  Then no SubTask is added.

Scenario: TCUS002N002-Check if no Subtask is added when its Due Date is incorrect.

  Given I want to check if invalid SubTask Due Date avoids SubTask creation.
  When I fill an invalid Due Date
  Then SubTask is not added.

Scenario: TCUS002N003-Check if no Subtask is added when its name is blank.
  
  Given I want to check if a blank SubTask name avoids SubTask creation.
  When I fill only Due Date
  Then SubTask is not added to list.

Scenario: TCUS002N004-Check if no Subtask is added when its Due Date is blank.
  
  Given I want to check if a blank SubTask Due Date avoids SubTask creation.
  When I fill only Task Name
  Then SubTask is not created.

Scenario: TCUS002N005-Check if no Subtask is added when its Due Date is invalid.

  Given I want to check if an invalid SubTask Due Date avoids SubTask creation.
  When I fill an invalid SubTask Due Date
  Then SubTask not created.
