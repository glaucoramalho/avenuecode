Given(/^I want to check if 'Manage subtasks' is visible for a task$/) do
  @app.home.load
  @app.home.signInLink.click
  @app.signin.userId.set 'glauco.ramalho@gmail.com'
  @app.signin.password.set 'senha1234'
  @app.signin.loginButton.click
  @app.userMainScreen.myTasksLink.click
end

When(/^I create a task$/) do
  @myTask = 'New task 2'
  @app.myTasks.newTask.taskName.set @myTask
  @app.myTasks.newTask.addTask.click 
end

When(/^the 'Manage subtasks' button is visible$/) do
  @manageButton = @app.myTasks.tasksTable.tasksButton.at(0)
  expect @manageButton.visible?
end

Then(/^the subtasks count is 0$/) do
  @manageButtonText = @manageButton.text
  @expectedButtonText = "(0) Manage Subtasks"
  expect(@expectedButtonText).to eq(@manageButtonText)
  @app.myTasks.navBar.myTasksLogOut.click
end

Given(/^I want to see if the 'Manage subtasks' popup is displayed$/) do
  @app.home.load
  @app.home.signInLink.click
  @app.signin.userId.set 'glauco.ramalho@gmail.com'
  @app.signin.password.set 'senha1234'
  @app.signin.loginButton.click
  @app.userMainScreen.myTasksLink.click
  @myTask = 'New task 2'
  @app.myTasks.newTask.taskName.set @myTask
  @app.myTasks.newTask.addTask.click 
end

When(/^I click in 'Manage Subtasks' button$/) do
  puts @app.myTasks.tasksTable.tasksButton.size
  save_and_open_screenshot
  @app.myTasks.tasksTable.tasksButton.at(0).click
end

Then(/^the 'Manage subtasks' popup is displayed$/) do
  expect @app.myTasks.subTaskForm.visible?
  @app.myTasks.subTaskForm.subTasksCloseButton.click
  @app.myTasks.navBar.myTasksLogOut.click
end

Given(/^I want to see if 'Task Description' is read only and matches 'Task Description'$/) do
  @app.home.load
  @app.home.signInLink.click
  @app.signin.userId.set 'glauco.ramalho@gmail.com'
  @app.signin.password.set 'senha1234'
  @app.signin.loginButton.click
  @app.userMainScreen.myTasksLink.click
end

When(/^I click 'Manage Subtasks'$/) do
  @randomTask = rand(@app.myTasks.tasksTable.tasks.size)
  @taskName = @app.myTasks.tasksTable.tasks.at(@randomTask).text
  @app.myTasks.tasksTable.tasksButton.at(@randomTask).click
end

And (/^'Task Description' is read only$/)do
  @readOnlyAttribute = @app.myTasks.subTaskForm.taskName['readonly']
  @subTaskTaskName = @app.myTasks.subTaskForm.taskName.value
  expect(@readOnlyAttribute).to be('readonly')
end

Then(/^'Task Description' matches his 'Task Description'$/) do
  expect(@taskName).to be(@subTaskTaskName)
end

Given(/^I want to create a new Subtask$/) do
  @app.home.load
  @app.home.signInLink.click
  @app.signin.userId.set 'glauco.ramalho@gmail.com'
  @app.signin.password.set 'senha1234'
  @app.signin.loginButton.click
  @app.userMainScreen.myTasksLink.click
end

When(/^I click 'Manage Subtasks' and popup is displayed$/) do
  @randomTask = rand(@app.myTasks.tasksTable.tasks.size)
  @app.myTasks.tasksTable.tasksButton.at(@randomTask).click
end

When(/^I fill 'Subtask Description'$/) do
  @subTaskName = 'SubTask'
  @app.myTasks.subTaskForm.subTaskName.set @subTaskName
end

When(/^I fill 'Subtask Due Date'$/) do
  @app.myTasks.subTaskForm.dueDate.set '04/06/2016'
end

When(/^I click 'Add'$/) do
  @app.myTasks.subTaskForm.addSubtask.click
end

Then(/^The new Subtask should be displayed in the bottom part of the modal.$/) do
  @currentSubTaskName =  @app.myTasks.subTaskForm.subTasks.at(0).text
  expect(@subTaskName).to eq(@currentSubTaskName)
end

Given(/^I want to check if invalid SubTask name avoids SubTask creation$/) do
  @app.home.load
  @app.home.signInLink.click
  @app.signin.userId.set 'glauco.ramalho@gmail.com'
  @app.signin.password.set 'senha1234'
  @app.signin.loginButton.click
  @app.userMainScreen.myTasksLink.click
  @randomTask = rand(@app.myTasks.tasksTable.tasks.size)
  @app.myTasks.tasksTable.tasksButton.at(@randomTask).click
end

When(/^I fill an incorrect SubTask name$/) do
  @subTaskName = '01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
  @app.myTasks.subTaskForm.subTaskName.set @subTaskName
  @app.myTasks.subTaskForm.dueDate.set '04/06/2016'
  @app.myTasks.subTaskForm.addSubtask.click
end

Then(/^no SubTask is added.$/) do
  @currentSubTaskName =  @app.myTasks.subTaskForm.subTasks.at(0).text
  expect(@subTaskName).not_to eq(@currentSubTaskName)
end

Given(/^I want to check if invalid SubTask Due Date avoids SubTask creation.$/) do
  @app.home.load
  @app.home.signInLink.click
  @app.signin.userId.set 'glauco.ramalho@gmail.com'
  @app.signin.password.set 'senha1234'
  @app.signin.loginButton.click
  @app.userMainScreen.myTasksLink.click
  @randomTask = rand(@app.myTasks.tasksTable.tasks.size)
  @app.myTasks.tasksTable.tasksButton.at(@randomTask).click
end

When(/^I fill an invalid Due Date$/) do
  @subTaskName = 'Incorrect SubTask Date'
  @app.myTasks.subTaskForm.subTaskName.set @subTaskName
  @app.myTasks.subTaskForm.dueDate.set '4/6/2016'
  @app.myTasks.subTaskForm.addSubtask.click
end

Then(/^SubTask is not added.$/) do
  @currentSubTaskName =  @app.myTasks.subTaskForm.subTasks.at(0).text
  expect(@subTaskName).not_to eq(@currentSubTaskName)
end

Given(/^I want to check if a blank SubTask name avoids SubTask creation.$/) do
  @app.home.load
  @app.home.signInLink.click
  @app.signin.userId.set 'glauco.ramalho@gmail.com'
  @app.signin.password.set 'senha1234'
  @app.signin.loginButton.click
  @app.userMainScreen.myTasksLink.click
  @randomTask = rand(@app.myTasks.tasksTable.tasks.size)
  @app.myTasks.tasksTable.tasksButton.at(@randomTask).click
end

When(/^I fill only Due Date$/) do
  @subTaskName = ''
  @app.myTasks.subTaskForm.subTaskName.set @subTaskName
  @app.myTasks.subTaskForm.dueDate.set '04/06/2016'
  @app.myTasks.subTaskForm.addSubtask.click
end

Then(/^SubTask is not added to list.$/) do
  @currentSubTaskName =  @app.myTasks.subTaskForm.subTasks.at(0).text
  expect(@subTaskName).not_to eq(@currentSubTaskName)
end

Given(/^I want to check if a blank SubTask Due Date avoids SubTask creation.$/) do
  @app.home.load
  @app.home.signInLink.click
  @app.signin.userId.set 'glauco.ramalho@gmail.com'
  @app.signin.password.set 'senha1234'
  @app.signin.loginButton.click
  @app.userMainScreen.myTasksLink.click
  @randomTask = rand(@app.myTasks.tasksTable.tasks.size)
  @app.myTasks.tasksTable.tasksButton.at(@randomTask).click
end

When(/^I fill only Task Name$/) do
  @subTaskName = 'Blank Due Date'
  @app.myTasks.subTaskForm.subTaskName.set @subTaskName
  @app.myTasks.subTaskForm.dueDate.set ''
  @app.myTasks.subTaskForm.addSubtask.click
end

Then(/^SubTask is not created.$/) do
  @currentSubTaskName =  @app.myTasks.subTaskForm.subTasks.at(0).text
  expect(@subTaskName).not_to eq(@currentSubTaskName)
end

Given(/^I want to check if an invalid SubTask Due Date avoids SubTask creation.$/) do
  @app.home.load
  @app.home.signInLink.click
  @app.signin.userId.set 'glauco.ramalho@gmail.com'
  @app.signin.password.set 'senha1234'
  @app.signin.loginButton.click
  @app.userMainScreen.myTasksLink.click
  @randomTask = rand(@app.myTasks.tasksTable.tasks.size)
  @app.myTasks.tasksTable.tasksButton.at(@randomTask).click
end

When(/^I fill an invalid SubTask Due Date$/) do
  @subTaskName = 'Invalid Due Date'
  @app.myTasks.subTaskForm.subTaskName.set @subTaskName
  @app.myTasks.subTaskForm.dueDate.set 'Invalid'
  @app.myTasks.subTaskForm.addSubtask.click
end

Then(/^SubTask not created.$/) do
  @currentSubTaskName =  @app.myTasks.subTaskForm.subTasks.at(0).text
  expect(@subTaskName).not_to eq(@currentSubTaskName)
end
