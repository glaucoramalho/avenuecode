 Given(/^I access 'Tasks' main screen$/)do
   @app.home.load
   @app.home.signInLink.click
 end
 When(/^I log into 'Tasks'$/)do
   @app.signin.userId.set 'glauco.ramalho@gmail.com'
   @app.signin.password.set 'senha1234'
   @app.signin.loginButton.click
 end

 And(/^'My Tasks' link is visible from 'Main Page'$/)do
   expect @app.userMainScreen.myTasksLink.visible?
 end

 And(/^'My Tasks' link is working$/)do
   @app.userMainScreen.myTasksLink.click
 end

 And(/^'My Tasks' link is visible from 'My Tasks' page$/)do
   expect @app.myTasks.navBar.myTasksLink.visible?
 end

 Then(/^'My Tasks' link is working from 'My Tasks' page.$/)do
   @app.myTasks.navBar.myTasksLink.click
   expect @app.myTasks.navBar.myTasksLink.visible?
 end

 Given(/^I want to add a new Task by clicking 'Add Task' button$/) do
   @app.home.load
   @app.home.signInLink.click
   @app.signin.userId.set 'glauco.ramalho@gmail.com'
   @app.signin.password.set 'senha1234'
   @app.signin.loginButton.click
   @app.userMainScreen.myTasksLink.click
 end

 When(/^I add a valid task name and click 'Add Task' button$/) do
   @myTask = 'New task 1'
   @app.myTasks.newTask.taskName.set @myTask
   @app.myTasks.newTask.addTask.click 
 end

 Then(/^the task should be added to tasks list\.$/) do
   currentTaskName = @app.myTasks.tasksTable.tasks.at(0).text
   expect(@myTask).to eq(currentTaskName)
 end

 Given(/^I want to add a new task by hitting 'Enter' key$/) do
   @app.home.load
   @app.home.signInLink.click
   @app.signin.userId.set 'glauco.ramalho@gmail.com'
   @app.signin.password.set 'senha1234'
   @app.signin.loginButton.click
   @app.userMainScreen.myTasksLink.click
 end

 When(/^I add a valid task name and hit 'Enter' key$/) do
   @myTask = 'New task 2'
   @app.myTasks.newTask.taskName.set @myTask
   @app.myTasks.newTask.taskName.native.send_keys(:return)
 end

 Then(/^the new task should be added to tasks list\.$/) do
   currentTaskName = @app.myTasks.tasksTable.tasks.at(0).text
   expect(@myTask).to eq(currentTaskName)
 end

 Given(/^I want to check welcome message$/) do
    @app.home.load
    @app.home.signInLink.click
    @app.signin.userId.set 'glauco.ramalho@gmail.com'
    @app.signin.password.set 'senha1234'
    @app.signin.loginButton.click
    @app.userMainScreen.myTasksLink.click
end

Then(/^the message "Hey 'user', this is your todo list for today:' is displayed on 'My Tasks' page.$/)do
   @expectedMessage = "Hey Glauco Ramalho, this is your todo list for today:"
   @currentMessage = @app.myTasks.welcomeMessage.text
   expect(@expectedMessage).to eq(@currentMessage)
end

Given(/^I want to add title task name less then 3 characters$/) do
   @app.home.load
   @app.home.signInLink.click
   @app.signin.userId.set 'glauco.ramalho@gmail.com'
   @app.signin.password.set 'senha1234'
   @app.signin.loginButton.click
   @app.userMainScreen.myTasksLink.click
end

When(/^I add a task name with less then 3 characters and click 'Add Task' button$/) do
   @myTask = 'nt'
   @app.myTasks.newTask.taskName.set @myTask
   @app.myTasks.newTask.addTask.click 
end

Then(/^the task should not be added to tasks list\.$/) do
  @currentTaskName = @app.myTasks.tasksTable.tasks.at(0).text
  expect(@myTask).not_to eq(@currentTaskName)
end

Given(/^I want to add title task name bigger then 250 characters$/) do
   @app.home.load
   @app.home.signInLink.click
   @app.signin.userId.set 'glauco.ramalho@gmail.com'
   @app.signin.password.set 'senha1234'
   @app.signin.loginButton.click
   @app.userMainScreen.myTasksLink.click
end

When(/^I add a task name with more then 250 characters and click 'Add Task' button$/) do
   @myTask = "01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"
   @app.myTasks.newTask.taskName.set @myTask
   @app.myTasks.newTask.addTask.click 
end

Then(/^the task should not be displayed in tasks list\.$/) do
  @currentTaskName = @app.myTasks.tasksTable.tasks.at(0).text
  expect(@myTask).not_to eq(@currentTaskName)
end
