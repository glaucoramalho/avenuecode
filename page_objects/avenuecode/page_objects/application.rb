# file: page_objects/avenuecode/page_objects/application.rb
#
module AvenueCode
  module PageObjects
    class Application
      def initialize
        @pages = {}
      end

      def home
        @pages[:home] ||= AvenueCode::PageObjects::Pages::HomePage.new
      end

      def signin
        @pages[:signin] ||= AvenueCode::PageObjects::Pages::SignInPage.new
      end
     
      def userMainScreen
        @pages[:userMainScreen] ||= AvenueCode::PageObjects::Pages::UserMainScreen.new
      end

      def myTasks
        @pages[:myTasks] ||= AvenueCode::PageObjects::Pages::TasksPage.new
      end
    end
  end
end

