# define our site's page
module AvenueCode
  module PageObjects
    module Pages
      class HomePage < SitePrism::Page
        set_url "http://qa-test.avenuecode.com/"
        element :signInLink,"ul.nav:nth-child(2) > li:nth-child(1) > a:nth-child(1)"
      end
    end
  end
end
