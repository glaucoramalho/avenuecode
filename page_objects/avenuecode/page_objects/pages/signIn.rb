# define our site's page
module AvenueCode
  module PageObjects
    module Pages
      class SignInPage < SitePrism::Page
        element :userId,"input[id='user_email']"
        element :password,"input[id='user_password']"
        element :loginButton,".btn"
      end
    end
  end
end
