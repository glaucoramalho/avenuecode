# define our site's page
module AvenueCode
  module PageObjects
    module Pages
      class UserMainScreen < SitePrism::Page
        element :tasksButton,".btn"
        element :myTasksLink,"ul.nav:nth-child(1) > li:nth-child(2) > a:nth-child(1)"
      end
    end
  end
end
