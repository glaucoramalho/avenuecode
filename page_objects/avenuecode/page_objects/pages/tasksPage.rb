# define our site's page
module AvenueCode
  module PageObjects
    module Pages
      class NavBar < SitePrism::Section
        element :myTasksLink,"ul.nav:nth-child(1) > li:nth-child(2) > a:nth-child(1)"
        element :myTasksLogOut,"ul.nav:nth-child(2) > li:nth-child(2) > a:nth-child(1)"
      end
      class NewTask < SitePrism::Section
        element :taskName,"input[id='new_task']"
        element :addTask,".input-group-addon"
      end
      class TasksTable < SitePrism::Section
        elements :tasks,".table tr.ng-scope td.task_body a"
        elements :tasksButton,".table tr.ng-scope td button"
      end
      class AddSubtask < SitePrism::Section
        element :title,".modal-title"
        element :taskName,"#edit_task"
        element :subTaskName,"input[id='new_sub_task']"
        element :dueDate,"input[id='dueDate']"
        element :addSubtask,"button[id='add-subtask']"
        elements :subTasks,".modal-body > div:nth-child(4) > .table tr.ng-scope td.task_body a"
        element :subTasksCloseButton,".modal-footer > button:nth-child(1)"
      end
      class TasksPage < SitePrism::Page
        set_url "http://qa-test.avenuecode.com/tasks"
        section :navBar,NavBar,".container-fluid"
        element :welcomeMessage,"h1"
        section :newTask,NewTask,".well"
        section :tasksTable,TasksTable,".table"
        section :subTaskForm,AddSubtask,".modal-dialog"
      end
    end
  end
end
